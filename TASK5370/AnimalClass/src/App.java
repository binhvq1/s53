public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal(1,"male");
        Animal animal2 = new Animal(2,"female");
        Animal animal3 = new Animal(3,"male");

        Duck duck = new Duck(animal1.age, animal1.gender, "vang");
        Fish fish = new Fish(animal2.age, animal2.gender, 2, false);
        Zebra zebra = new Zebra(animal3.age, animal3.gender, true);

        System.out.println(duck.toString());
        System.out.println(fish.toString());
        System.out.println(zebra.toString());
    }
}
