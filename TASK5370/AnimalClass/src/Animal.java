public class Animal {
    int age;
    String gender;

    
    public Animal() {
        super();
    }

    
    public Animal(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }
    public String getGender() {
        return gender;
    }


    @Override
    public String toString() {
        return "Animal [age=" + age + ", gender=" + gender + "]";
    }
}
