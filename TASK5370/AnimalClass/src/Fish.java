public class Fish extends Animal {
    private int size = 1;
    private boolean canEat = true;

    
    public Fish(int size, boolean canEat) {
        this.size = size;
        this.canEat = canEat;
    }
    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public boolean isCanEat() {
        return canEat;
    }
    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }
    @Override
    public String toString() {
        return String.format("Fish [Age = %s, Gender = %s, canEat= %s, size= %s]", age, gender, canEat,size);
    }

    
}
