public class Duck extends Animal {
    private String beakColor;

    public Duck(String beakColor) {
        this.beakColor = beakColor;
    }

    public Duck(int age, String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }

    public String getBeakColor() {
        return beakColor;
    }

    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }

    @Override
    public String toString() {
        return String.format("Duck [Age = %s, Gender = %s, beakColor= %s]", age, gender, beakColor);
    }

    
}
