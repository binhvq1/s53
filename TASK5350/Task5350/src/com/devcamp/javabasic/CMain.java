package com.devcamp.javabasic;

import java.util.ArrayList;

import com.devcamp.javabasic.interfaceclass.ISumable;

public class CMain {

	public static void main(String[] args) {
		CPet dog = new CDog();
		CPet cat = new CCat();

		ArrayList<CPet> petsList = new ArrayList<>();
		petsList.add(dog);
		petsList.add(cat);

		CPerson person1 = new CPerson();
		CPerson person2 = new CPerson(1,20,"Le Van","Thang",petsList);

		System.out.println("Full Name = " + person1.getFirstName());
		System.out.println("Full Name = " + person2.getFirstName());
		System.out.println("age = " + person2.getAge());
	}	
}

