package com.devcamp.javabasic.interfaceclass;

public interface ISumable {
	String  getSum();
}
