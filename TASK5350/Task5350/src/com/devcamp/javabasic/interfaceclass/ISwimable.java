package com.devcamp.javabasic.interfaceclass;

public interface ISwimable {
	void swim();
}
