package com.devcamp.javabasic.interfaceclass;

public interface IFlyable {
	void fly();
}
