package com.devcamp.javabasic.interfaceclass;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}
