package com.devcamp.javabasic.interfaceclass;
public interface IRunable extends IBarkable {
	void run() ;
	void running() ;
}
