package com.devcamp.javabasic;

import java.util.ArrayList;
import java.util.Iterator;

import com.devcamp.javabasic.interfaceclass.ISumable;

public class CIntegerArrayList implements ISumable {
	/**
	 * @param mIntegerArrayList
	 */
	public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}

	private ArrayList<Integer> mIntegerArrayList;

	@Override
	public String getSum() {
		int sum = 0;
		for (Iterator iterator = this.mIntegerArrayList.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			sum += integer.intValue();
		}
		return "Đây là Sum của class CIntegerArrayList: "+sum;
	}

	/**
	 * @return the mIntegerArrayList
	 */
	public ArrayList<Integer> getmIntegerArrayList() {
		return mIntegerArrayList;
	}

	/**
	 * @param mIntegerArrayList the mIntegerArrayList to set
	 */
	public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
		this.mIntegerArrayList = mIntegerArrayList;
	}
 
}
