package com.devcamp.javabasic;

import com.devcamp.javabasic.interfaceclass.ISumable;

public class CIntArray implements ISumable {
	/**
	 * @param mIntArray
	 */
	public CIntArray(int[] mIntArray) {
		this.mIntArray = mIntArray;
	}
	private int [ ] mIntArray;
	
	@Override
	public String getSum() {
		int sum = 0;
		for (int i = 0; i < mIntArray.length; i++) {
			sum += mIntArray[i];
		}
		return "Đây là Sum của class CIntArray: "+sum;
	}
	/**
	 * @return the mIntArray
	 */
	public int[] getmIntArray() {
		return mIntArray;
	}
	/**
	 * @param mIntArray the mIntArray to set
	 */
	public void setmIntArray(int[] mIntArray) {
		this.mIntArray = mIntArray;
	}

}
