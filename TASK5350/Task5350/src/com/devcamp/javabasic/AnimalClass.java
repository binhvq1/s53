package com.devcamp.javabasic;

public enum AnimalClass {
	fish, amphibians, reptiles, mammals, birds
}
