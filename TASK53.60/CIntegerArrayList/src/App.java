import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> mIntegerArrayList = new ArrayList<Integer>();
        mIntegerArrayList.add(1);
        mIntegerArrayList.add(2);
        mIntegerArrayList.add(3);
        mIntegerArrayList.add(4);
        mIntegerArrayList.add(5);

        CIntegerArrayList sumArrList = new CIntegerArrayList(mIntegerArrayList);

        System.out.println(sumArrList.getSum());

    }
}
