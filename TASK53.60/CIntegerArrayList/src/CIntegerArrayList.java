import java.util.ArrayList;

public class CIntegerArrayList implements ISumable {
    ArrayList<Integer> mIntegerArrayList = new ArrayList<>();

    public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
        this.mIntegerArrayList = mIntegerArrayList;
    }

    
    public String getSum() {
        int sum = 0;
        for(int i = 0; i < mIntegerArrayList.size(); i++)
                sum += mIntegerArrayList.get(i);
        return String.format("Day la tong gia tri Sum cua CIntegerArrayList =  %s", sum);
    }    
}
